import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fromJS, Map } from 'immutable'

import Loader from 'react-loader-spinner'
import * as actions from '../../store/actions/index'
import Input from '../Input/Input'
import { convertBase, checkValidity } from '../../utils/index'
import Controls from '../Controls/Controls'
import callApi from '../../utils/callApi'
import ConfirmArray from '../ConfirmArray/ConfirmArray'

const minLenght = {
  dec: 1,
  hex: 1
}

class VisitorItem extends Component {
  state = {
    mask: {
      hex: {
        mask: 'FFFF-FFFF',
        maskChar: ''
      },
      dec: {
        mask: '9999999999',
        maskChar: ''
      },
      formatChars: {
        9: '[0-9]',
        F: '[A-Fa-f0-9]'
      }
    },
    editVisitor: false,
    copyForm: new Map({}),
    form: new Map({}),
    formIsValid: true,
    confirmOverride: {}
  }

  componentWillMount () {
    const { data } = this.props
    this.setState({
      copyForm: this.createDataInput(data, data.name === ''),
      form: this.createDataInput(data, data.name === ''),
      formIsValid: data.name !== ''
    })
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevProps.update !== this.props.update) {
      const { data } = this.props
      this.setState({ copyForm: this.createDataInput(data), form: this.createDataInput(data) })
    }
  }

  createDataInput = (data, isNew = false) => {
    const { name, serialNumber, switch: switchInput } = data
    const block = {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          readOnly: true
        },
        value: name,
        validation: {
          required: true
        },
        label: 'Nickname',
        valid: !isNew,
        touched: true,
        edit: false

      },
      serialNumber: {
        elementType:
          'inputHEX',
        elementConfig: {
          type: 'text',
          readOnly: true
        },
        value: serialNumber === null ? '' : serialNumber,
        validation: {
          required: true,
          minLength: minLenght.dec,
          maxLength: 10,
          mask: this.state.mask.dec.mask,
          maskChar: this.state.mask.dec.maskChar
        },
        label: 'Wristband',
        valid: !isNew,
        touched: true,
        edit: false
      },
      formatSerialNumber: {
        elementType:
          'checkbox',
        elementConfig: {
          type: 'checkbox',
          checked: false,
          readOnly: true
        },
        value: 'DEC',
        validation: {
          required: false
        },
        label: 'Hex',
        valid: true,
        touched: true
      },
      override: {
        elementType:
          'checkbox',
        elementConfig: {
          type: 'checkbox',
          checked: false,
          readOnly: true
        },
        value: false,
        validation: {
          required: false
        },
        label: 'Override',
        valid: true,
        touched: true
      },
      switch: {
        elementType:
          'checkbox',
        elementConfig: {
          type: 'checkbox',
          // checked: switchInput,
          checked: 'true',
          readOnly: true
        },
        // value: switchInput,
        value: 'true',
        validation: {
          required: false
        },
        label: 'Switch',
        valid: true,
        touched: true
      }
    }
    return fromJS(block)
  }

  inputChangedHandler = (event, key) => {
    if (key === 'formatSerialNumber') {
      let uptForm = this.state.form
      const maskHEX = this.state.mask.hex
      const maskDEC = this.state.mask.dec

      const checked = !uptForm.getIn([key, 'elementConfig', 'checked'])

      uptForm = uptForm.updateIn(
        ['formatSerialNumber', 'elementConfig', 'checked'],
        (checked) => !checked
      )

      uptForm = uptForm.updateIn(
        ['formatSerialNumber', 'value'],
        (value) => (checked ? 'HEX' : 'DEC')
      )

      uptForm = uptForm.updateIn(
        [key, 'edit'],
        (edit) => true
      )

      uptForm = uptForm.updateIn(
        ['serialNumber', 'validation', 'mask'],
        (mask) => (checked ? maskHEX.mask : maskDEC.mask)
      )

      uptForm = uptForm.updateIn(
        ['serialNumber', 'validation', 'maskChar'],
        (maskChar) => (checked ? maskHEX.maskChar : maskDEC.maskChar)
      )

      uptForm = uptForm.updateIn(
        ['serialNumber', 'validation', 'minLength'],
        (minLength) => (checked ? minLenght.hex : minLenght.dec)
      )

      uptForm = uptForm.updateIn(
        ['serialNumber', 'value'],
        (value) => ''
      )

      uptForm = uptForm.updateIn(
        ['serialNumber', 'valid'],
        (valid) => false
      )

      return this.setState({ form: uptForm, formIsValid: false })
    }
    if (key === 'switch') {
      let uptForm = this.state.form

      const checkedNew = !uptForm.getIn([key, 'elementConfig', 'checked'])

      uptForm = uptForm.updateIn(
        ['switch', 'elementConfig', 'checked'],
        (checked) => checkedNew
      )

      uptForm = uptForm.updateIn(
        ['switch', 'value'],
        (value) => (checkedNew ? 'true' : 'false')
      )

      uptForm = uptForm.updateIn(
        [key, 'edit'],
        (edit) => true
      )

      return this.setState({ form: uptForm })
    }
    const valueInput = key === 'serialNumber' ? event.target.value.toUpperCase() : event.target.value

    let uptForm = this.state.form.updateIn([key, 'value'], (value) => valueInput)

    uptForm = uptForm.updateIn(
      [key, 'edit'],
      (edit) => true
    )
    if (key === 'override') {
      const checked = uptForm.getIn([key, 'elementConfig', 'checked'])

      uptForm = uptForm.updateIn(
        [key, 'value'],
        (value) => String(!checked)
      )

      uptForm = uptForm.updateIn(
        [key, 'elementConfig', 'checked'],
        (checked) => !checked
      )
    }

    uptForm = uptForm.updateIn([key, 'valid'], (valid) => (
      checkValidity(event.target.value, this.state.form.getIn([key, 'validation']).toJS())
    ))
    uptForm = uptForm.updateIn([key, 'touched'], (touched) => true)

    let formIsValid = true

    uptForm.forEach((confMap, id) => {
      const config = confMap.toJS()
      formIsValid = config.valid && formIsValid
    })

    this.setState({ form: uptForm, formIsValid })
  }

  pad = (str, max) => {
    str = str.toString()
    return str.length < max ? this.pad(`0${str}`, max) : str
  }

  readOnly = (onlyRead) => {
    let uptForm = this.state.form
    this.state.form.forEach((_, inputName) => {
      uptForm = uptForm.updateIn(
        [inputName, 'elementConfig', 'readOnly'],
        (readOnly) => onlyRead
      )
    })
    this.setState(() => ({ form: uptForm }))
  }

  resetEdit = () => {
    let uptForm = this.state.form
    this.state.form.forEach((_, inputName) => {
      uptForm = uptForm.updateIn(
        [inputName, 'edit'],
        (edit) => false
      )
    })
    this.setState(() => ({ form: uptForm }))
  }

  /** ***********************************************************
   *                  HANDLER
   ************************************************************ */
  handleCancelVisitor = (idVisitor) => () => {
    this.readOnly(true)
    this.setState(() => ({ editVisitor: false, form: this.state.copyForm, formIsValid: this.checkValidity() }))
  }

  checkValidity = () => {
    let formIsValid = true
    this.state.copyForm.forEach((confMap, id) => {
      const config = confMap.toJS()
      formIsValid = config.valid && formIsValid
    })
    return formIsValid
  }

  handleEditVisitor = (idVisitor) => () => {
    const { editVisitor } = this.state

    if (!editVisitor) { // starts edit visitor
      this.readOnly(false)
      this.setState({ editVisitor: true })
    } else {
      this.setState({ load: true })
      this.sendSaveVisitor()
    }
  }

  handleDeleteConfirm = (idVisitor) => () => {
    const name = this.state.form.getIn(['name', 'value'])
    const payload = {
      text: `Delete visitor ${name}`,
      callback: (status) => this.handleDeleteVisitor(status),
      isOpen: true
    }
    this.props.optionConfirm(payload)
  }

  handleDeleteVisitor = (status) => {
    this.setState({ isConfirm: false })
    if (!status) return
    const { id } = this.props

    const email = this.props.emailVisitor
    const data = {
      bouncerId: id,
      email
    }
    this.setState({ load: true })
    callApi('post', '', data)
      .then((res) => {
        if (res.status) {
          this.setState({ load: false })
          this.props.deleteVisitor(id)
        }
      })
      .catch((er) => {
        this.setState({ load: false })
      })
  }

  handleOverrid = (status, payload) => {
    this.setState({ confirmOverride: {} })
    if (!status) return
    const { form } = this.state
    let uptForm = form
    uptForm = uptForm.updateIn(
      ['override', 'value'],
      (value) => (status ? true : 'null')
    )

    this.setState({ form: uptForm }, () => {
      this.sendSaveVisitor(true)
    })
  }

  /** ***********************************************************
   *                  HANDLER END
   ************************************************************ */
  sendSaveVisitor = (allUpdate) => {
    let visitor = {}
    const userEmail = this.props.emailVisitor
    this.state.form.forEach((input, index) => {
      let value = input.get('value')
      if (index === 'serialNumber') {
        value = value.replace('-', '')
      }
      visitor = Object.assign(visitor, {
        [index]: value
      })
    })
    visitor.id = this.props.id
    visitor.switch = visitor.switch === 'true'
    // console.log(this.state.form.toJS())

    const data = {
      userEmail,
      formatSerialNumber: visitor.formatSerialNumber,
      visitor
    }
    callApi('post', '', data)
      .then((res) => {
        if (Object.keys(res.data).length !== 0) {
          const overrideList = res.data
          this.setState({ confirmOverride: overrideList, load: false, editVisitor: true })
        } else {
          this.readOnly(true)
          this.setState({ editVisitor: false, load: false, copyForm: this.state.form })
          this.resetEdit()
          if (allUpdate) { this.props.fetchVisitors() }
        }
      })
      .catch((er) => {
        this.setState({ load: false, editVisitor: true })
        if (allUpdate) { this.props.fetchVisitors() }
      })
  }

  switchSerial = (code) => {
    if (code === '') return ''
    let res = ''
    for (let s = 0; s < code.length; s += 2) {
      if (code[s + 1]) {
        res = code[s] + code[s + 1] + res
      } else {
        res = code[s] + res
      }
    }
    return res
  }

  render () {
    const { editVisitor, load, confirmOverride } = this.state
    const { inputChangedHandler } = this
    const { formatChars } = this.state.mask
    const handleVideoShow = () => this.props.handleVideoShow(this.state.form.getIn(['name', 'value']))
    const { id } = this.props
    const {
      name,
      serialNumber,
      formatSerialNumber,
      switch: switchInput
    } = this.state.form.toJS()
    console.log('switchInput', switchInput)

    let serialNumberValue = serialNumber.value.replace('-', '')
    let convertSerialNumber = ''
    if (formatSerialNumber.value === 'DEC' && serialNumber.value !== '') {
      convertSerialNumber = convertBase(serialNumberValue).from(10).to(16)
      convertSerialNumber = this.pad(convertSerialNumber.toUpperCase(), 8)
      if (switchInput.elementConfig.checked) {
        convertSerialNumber = this.switchSerial(convertSerialNumber)
      }
      convertSerialNumber = this.pad(convertSerialNumber.toUpperCase(), 8)
      convertSerialNumber = `${convertSerialNumber.slice(0, 4)}-${convertSerialNumber.slice(4)}`
      convertSerialNumber = convertSerialNumber.toUpperCase()
    } else if (formatSerialNumber.value === 'HEX' && serialNumber.value !== '') {
      if (switchInput.elementConfig.checked) {
        serialNumberValue = this.switchSerial(serialNumberValue)
      }
      convertSerialNumber = convertBase(serialNumberValue).from(16).to(10)

      convertSerialNumber = this.pad(convertSerialNumber.toUpperCase(), 10)
    }
    let renderConfirmOverride = []
    if (Object.keys(confirmOverride).length !== 0) {
      renderConfirmOverride = (
        <ConfirmArray callBack={this.handleOverrid} addInfo={id}>
          {`Wristband ${confirmOverride.serialNumber} belongs to the visitor ${confirmOverride.oldVisitorNickName} user ${confirmOverride.oldUserEmail}. Override?`}
        </ConfirmArray>
      )
    }
    return (
      <div className="visitor__item">
        {renderConfirmOverride}
        <div className="wrapper-load">
          {load &&
          (
            <div className="wrapper-load__load">
              <Loader
                type="RevolvingDot"
                color="#000000"
                height="50"
                width="50"
              />
            </div>
          )}
          <div className="flex-row">
            <Input
              elementType={name.elementType}
              value={name.value}
              elementConfig={name.elementConfig}
              invalid={!name.valid}
              shouldValidate={name.validation}
              touched={name.touched}
              label={name.label}
              edit={name.edit}
              validation={name.validation}
              formatChars={formatChars}
              changed={(event) => inputChangedHandler(event, 'name')}
            />
          </div>
          <div className="flex-row">
            <Input
              elementType={serialNumber.elementType}
              value={serialNumber.value}
              elementConfig={serialNumber.elementConfig}
              invalid={!serialNumber.valid}
              shouldValidate={serialNumber.validation}
              touched={serialNumber.touched}
              label={serialNumber.label}
              edit={serialNumber.edit}
              validation={serialNumber.validation}
              formatChars={formatChars}
              changed={(event) => inputChangedHandler(event, 'serialNumber')}
            />
            <div className="converter">
              <span className="converter__sep">=</span>
              {convertSerialNumber}
            </div>
          </div>
          <div className="flex-row">
            <Input
              elementType={formatSerialNumber.elementType}
              value={formatSerialNumber.value}
              elementConfig={formatSerialNumber.elementConfig}
              invalid={!formatSerialNumber.valid}
              shouldValidate={formatSerialNumber.validation}
              touched={formatSerialNumber.touched}
              label={formatSerialNumber.label}
              edit={formatSerialNumber.edit}
              validation={formatSerialNumber.validation}
              formatChars={formatChars}
              changed={(event) => inputChangedHandler(event, 'formatSerialNumber')}
            />
          </div>
          {/* <div className="flex-row"> */}
          {/*  <Input */}
          {/*    elementType={switchInput.elementType} */}
          {/*    value={switchInput.value} */}
          {/*    elementConfig={switchInput.elementConfig} */}
          {/*    invalid={!switchInput.valid} */}
          {/*    shouldValidate={switchInput.validation} */}
          {/*    touched={switchInput.touched} */}
          {/*    label={switchInput.label} */}
          {/*    edit={switchInput.edit} */}
          {/*    validation={switchInput.validation} */}
          {/*    formatChars={formatChars} */}
          {/*    changed={(event) => inputChangedHandler(event, 'switch')} */}
          {/*  /> */}
          {/* </div> */}
          <Controls
            idElement={id}
            className="visitor__item__panel"
            isEdit={editVisitor}
            handleRemove={this.handleDeleteConfirm}
            handleCancel={this.handleCancelVisitor}
            handleEdit={this.handleEditVisitor}
            handleVideo={handleVideoShow}
            editDisabled={this.state.formIsValid}
          />
        </div>
      </div>
    )
  }
}

VisitorItem.propTypes = {
  closeConfirm: PropTypes.func,
  data: PropTypes.shape({
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    name: PropTypes.string,
    serialNumber: PropTypes.string,
    switch: PropTypes.bool.isRequired
  }),
  deleteVisitor: PropTypes.func,
  emailVisitor: PropTypes.string,
  fetchVisitors: PropTypes.func,
  handleVideoShow: PropTypes.func,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  openConfirm: PropTypes.func,
  optionConfirm: PropTypes.func,
  update: PropTypes.bool
}
const mapDispatchToProps = (dispatch) => ({
  openConfirm: () => dispatch(actions.openConfirm()),
  closeConfirm: () => dispatch(actions.closeConfirm()),
  optionConfirm: (payload) => dispatch(actions.optionConfirm(payload))
})
export default connect(null, mapDispatchToProps)(VisitorItem)
