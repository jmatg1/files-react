import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { fromJS, Map } from 'immutable'
import Loader from 'react-loader-spinner'
import callApi from '../../utils/callApi'
import VisitorItem from '../../components/VisitorItem/VisitorItem'
import Button from '../../components/Button/Button'

class VisitorList extends Component {
  state = {
    form: null, // new Map()
    load: true,
    updateVisitor: true
  }

  componentDidMount () {
    const newForm = new Map({})
    this.setState({ form: newForm }, () => this.fetchVisitors())
  }

  componentWillUnmount () {

  }

  fetchVisitors = () => {
    this.setState({ load: true })
    const email = this.props.emailVisitor

    callApi('get', ``)
      .then((res) => {
        if (res.data.length > 0) {
          let uptForm = new Map({})

          res.data.forEach((visitor) => {
            uptForm = uptForm.set(Number(visitor.id), fromJS(visitor))
          })
          uptForm = uptForm.sortBy((f, i) => i)
          return this.setState((prev) => ({ form: uptForm, load: false, updateVisitor: !prev.updateVisitor }))
        }
        this.setState({ load: false })
      })
      .catch((er) => {
        this.setState({ load: false })
      })
  }

  deleteVisitor = (id) => {
    let uptForm = this.state.form.delete(Number(id))
    uptForm = uptForm.sortBy((f, i) => i)
    this.setState({ form: uptForm })
  }

  handleAddVisitor = () => {
    const visitors = this.state.form

    let newId = visitors.size
    const allId = []

    visitors.forEach((__, id) => {
      allId.push(id)
    })

    for (let i = 0; i < visitors.size; i++) {
      if (allId.find((id) => i === id) === undefined) newId = i
    }

    const newVisitor = {
      id: newId,
      name: '',
      serialNumber: '',
      switch: false
    }
    const uptForm = this.state.form.set(newId, fromJS(newVisitor))
    this.setState({ form: uptForm, formIsValid: false })
  }

  render () {
    const { form, load } = this.state
    const { emailVisitor } = this.props
    const renderVisitors = []
    if (form) {
      form.forEach((visitor, id) => {
        renderVisitors.push(<VisitorItem
          key={id}
          id={id}
          data={visitor.toJS()}
          handleVideoShow={this.props.handleVideoShow}
          emailVisitor={emailVisitor}
          deleteVisitor={this.deleteVisitor}
          fetchVisitors={this.fetchVisitors}
          update={this.state.updateVisitor}
        />)
      })
    }
    return (
      <div className="visitor__list">
        <div className="wrapper-load wrapper-load--visitors">
          {load &&
          (
          <div className="wrapper-load__load">
            <Loader
              type="RevolvingDot"
              color="#000000"
              height="50"
              width="50"
            />
          </div>
          )}
          <Button handleClick={this.handleAddVisitor}>+ Add Visitor</Button>
          {renderVisitors}
        </div>
      </div>
    )
  }
}

VisitorList.propTypes = {
  emailVisitor: PropTypes.string,
  handleVideoShow: PropTypes.func
}

export default VisitorList
