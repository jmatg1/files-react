import React from 'react'

export default () => (
  <div className="icon-search">
    <svg xmlns="http://www.w3.org/2000/svg" width="14.377" height="14.39" viewBox="0 0 14.377 14.39">
      <g id="Group_6" data-name="Group 6" transform="translate(0.5 0.5)">
        <circle id="Oval_3" data-name="Oval 3" cx="5.671" cy="5.671" r="5.671" fill="none" stroke="#979797" strokeMiterlimit="10" strokeWidth="1"/>
        <path id="Line_4" data-name="Line 4" d="M0,0,3.4,3.416" transform="translate(9.766 9.766)" fill="none" stroke="#979797" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="1"/>
      </g>
    </svg>
  </div>
)
