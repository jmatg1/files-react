import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ClickHandler extends Component {
  state = {
    isVisible: false,
    searchText: '',
  };

  static propTypes = {
    children: PropTypes.func.isRequired,
  };

  componentDidMount () {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = ({ target }) => {
    if (
      this.state.isVisible &&
      this.wrapperRef &&
      !this.wrapperRef.contains(target)
    ) {
      this.handleSelectClose()
    }
  };

  handleInputChange = ({ target: { value } }) => {
    this.setState({ searchText: value, isVisible: true })
  };

  handleSelectClose = () => {
    this.setState({ isVisible: false })
  };

  handleSelectClick = () => {
    this.setState(prevState => ({ isVisible: !prevState.isVisible }))
  };

  handleOptionSelect = props => {
    this.setState({ isVisible: false, searchText: '' })
  };

  render = () => (
    <div key={0} ref={node => (this.wrapperRef = node)} className={this.state.isVisible ? 'select-open' : 'select-close'}>
      {this.props.children({
        isVisible: this.state.isVisible,
        handleInputChange: this.handleInputChange,
        handleSelectClick: this.handleSelectClick,
        handleOptionSelect: this.handleOptionSelect,
        handleSelectClose: this.handleSelectClose,
        searchText: this.state.searchText,
      })}
    </div>
  );
}
