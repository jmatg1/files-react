import React from 'react'
import PropTypes from 'prop-types'
import ChevronIcon from './searchIcon'

const Selection = (props) => {
  const {
    handleSelectClick,
    isVisible,
    placeholder,
    selectedOptions,
  } = props
  let renderSelectedOption = placeholder
  if (selectedOptions.list.length > 0 && !props.withButtons) {
    renderSelectedOption = selectedOptions.list[0].displayValue
    if (selectedOptions.list.length > 1) { renderSelectedOption += ', ' + selectedOptions.list[1].displayValue }
    if (selectedOptions.list.length > 2) { renderSelectedOption += ',...' }
    if (selectedOptions.all === selectedOptions.list.length) { renderSelectedOption = 'All' }
  }
  return (
    <div className={`input-container ${isVisible ? 'focused' : ''}`}>
      <div className="select" onClick={handleSelectClick}>
        <span className="select__text">{renderSelectedOption}</span>
        <ChevronIcon isVisible={isVisible}/>
      </div>
    </div>
  )
}

Selection.propTypes = {
  handleInputChange: PropTypes.func.isRequired,
  handleSelectClick: PropTypes.func.isRequired,
  hasIcon: PropTypes.bool,
  isVisible: PropTypes.bool.isRequired,
  isSearchable: PropTypes.bool,
  // selectedOptions: PropTypes.arrayOf(PropTypes.object),
  placeholder: PropTypes.string,
  searchText: PropTypes.string,
}

export default Selection
