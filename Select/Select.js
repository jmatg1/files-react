import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ClickHandler from './ClickHandler'
import Selection from './Selection'
import Options from './Options'
import { arrToMap } from '../../share/utility'
import './select.scss'

import { Map, fromJS } from 'immutable'
import classNames from 'classnames'
import _ from 'lodash'

class Select extends Component {
  state = {
    selectOptions: new Map({}),
    selectedOptions: {
      all: null,
      list: [],
    },
    notChange: true,
  }

  componentDidMount () {
    this.setOptions()
  }

  setOptions = () => {
    const { selectOptions, defaultValues } = this.props

    const selectOptionsJS = selectOptions

    if (selectOptionsJS.length > 0 && this.props.multiple) {
      selectOptionsJS.unshift({
        id: -1,
        value: 'All',
        displayValue: 'All',
      })
    }

    let selectMap = arrToMap(selectOptionsJS, fromJS)
    selectMap = selectMap.map(opt => (
      opt.set('checked', this.props.selectedAll)
    ))
    /**
     * Select the default values that were specified in the prop.
     * The structure is the same as the list of options itself.
     **/
    if (defaultValues && defaultValues.length > 0) {
      defaultValues.forEach(item => {
        selectMap = selectMap.map(opt => {
          if (opt.get('id') === item.id) {
            return opt.update('checked', (checked) => true)
          } else {
            return opt
          }
        })
      })
    }

    const uptSelectedOptions = this.state.selectedOptions
    uptSelectedOptions.list = selectMap.filter(opt => opt.get('checked') === true).valueSeq().toJS()
    uptSelectedOptions.all = selectMap.size
    this.setState({
      selectOptions: selectMap,
      selectedOptions: uptSelectedOptions,
    })
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (!_.isEqual(this.props.selectOptions, prevProps.selectOptions)){
      this.setOptions()
    }
  }

  onOptionSelect = (id) => {
    if (this.state.notChange) {
      this.setState({ notChange: false})
    }
    if (id === undefined) return
    let uptSelectOptions = this.state.selectOptions
    const uptChecked = !uptSelectOptions.getIn([id, 'checked'])

    if (!this.props.multiple) {
      uptSelectOptions = uptSelectOptions.map(field => field.update('checked', () => false))
    }
    uptSelectOptions = uptSelectOptions.updateIn([id, 'checked'], checked => uptChecked)
    // for all option
    let selectCount = 0
    if  (id === -1) {
      uptSelectOptions.map((el, key) => {
        if (key !== -1 && el.getIn([key, 'ckecked']) === true){
          selectCount++
        }
        uptSelectOptions = uptSelectOptions.updateIn([key, 'checked'], checked => uptChecked)
      })
    }
    const uptSelectedOptions = {}
    uptSelectedOptions.list = uptSelectOptions.filter((opt, key) => opt.get('checked') && key !== -1).valueSeq().toJS()
    uptSelectedOptions.all = uptSelectOptions.size

    if (uptSelectedOptions.list.length === uptSelectedOptions.all - 1) {
      uptSelectOptions = uptSelectOptions.updateIn([-1, 'checked'], checked => true)
    } else {
      uptSelectOptions = uptSelectOptions.updateIn([-1, 'checked'], checked => false)
    }
    //

    // for all end

    this.setState({ selectOptions: uptSelectOptions, selectedOptions: uptSelectedOptions }, () => {
      this.props.handleChange && this.props.handleChange(this.state.selectedOptions)
    })
    if (this.props.onChange) {
      this.props.onChange(uptSelectedOptions)
    }
  }

  render () {
    const {
      selectOptions,
      selectedOptions,
    } = this.state
    const {
      handleChange,
      name,
      label,
      selectOptions: selectOptionsJS,
      value,
      square = false,
      withButtons,
      ...props
    } = this.props
    const classesSelect = classNames([
      'select-container',
      { 'not-empty': selectedOptions.list.length !== 0 && !withButtons },
      { square: square },
      { 'not-multiple': !this.props.multiple },
      { multiple: this.props.multiple },
    ])
    // do not show the country button until the user changes the country
    let renderButtons = selectedOptions.list.filter(el => {
      if (el.displayValue === 'Thailand' && this.state.notChange){
        return false
      }
      return true
    })
    renderButtons = renderButtons.map((el, i) => (
      <li className="multiselect-btn" key={i}>
      <span>{el.displayValue}</span>
      <div>
      <button className="rw-multiselect-tag-btn rw-btn rw-btn-select" onClick={() => this.onOptionSelect(el.id)}>
  <span>×</span>
    </button>
    </div>
    </li>
  ))
    return (
      <ClickHandler handleChange={handleChange} selectOptions={selectOptions}>
      {handlers => (
    <div className={classesSelect} key={1}>
      { withButtons &&
      <ul className="rw-multiselect-btn-list">
      {renderButtons}
      </ul>
  }
    {/* <Label>{label}</Label> */}
  <div className="select-box">
      <Selection withButtons={withButtons} {...handlers} {...props} selectedOptions={selectedOptions} />
    <Options
    {...props}
    {...handlers}
    name={name}
    selectOptions={selectOptions}
    onOptionSelect={this.onOptionSelect}
    />
    </div>
    </div>
  )}
  </ClickHandler>
  )
  }
}

Select.propTypes = {
  handleChange: PropTypes.func,
  onChange: PropTypes.func, // Dynamic select data. Use if options can be removed or new ones added.
  label: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  selectOptions: PropTypes.arrayOf(PropTypes.object.isRequired),
  value: PropTypes.arrayOf(PropTypes.object.isRequired),
  isSearchable: PropTypes.bool,
  multiple: PropTypes.bool,
  selectedAll: PropTypes.bool,
  withButtons: PropTypes.bool,
  defaultValues: PropTypes.arrayOf(PropTypes.object.isRequired),
}

export default Select
