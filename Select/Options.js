import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import SearchIcon from './searchIcon'

export default class Options extends PureComponent {
  static propTypes = {
    onOptionSelect: PropTypes.func.isRequired,
    isVisible: PropTypes.bool.isRequired,
    name: PropTypes.string,
    searchText: PropTypes.string,
    // selectOptions: PropTypes.objectOf(PropTypes.object.isRequired) // immutableJS
  };

  handleClickSelect = (id) => () => {
    if (!this.props.multiple) { this.props.handleSelectClose() }
    this.props.onOptionSelect(id)
  }

  render = () => {
    const {
      isVisible,
      isSearchable = false,
      searchText,
      selectOptions,
      placeholderSearch,
      handleInputChange,
      multiple,
    } = this.props

    const options = !searchText
      ? selectOptions
      : selectOptions.filter(opt => {
          return opt.get('displayValue').toLowerCase().includes(searchText.toLowerCase())
        }
      )

    return (
      <>
        {isVisible && (
          <div className="drop-container">
            <div className="options">
              {isSearchable && (
                <div className='select-search'>
                  <SearchIcon />
                  <input
                    className="search-option"
                    type="text"
                    placeholder={placeholderSearch}
                    onChange={handleInputChange}
                    value={searchText}
                  />
                </div>
              )}
              {options && options.size > 0 ? (
                options.map((field) => {
                    const optConfig = field.toJS()
                    let classesOpt = 'option'
                    if (!multiple && optConfig.checked) {
                      classesOpt += ' option-active'
                    }
                    return (
                      <div
                        className={classesOpt}
                        key={optConfig.id}
                        onClick={this.handleClickSelect(optConfig.id)}
                      >
                        <div className="checkbox_default">
                          <label htmlFor="download" className="checkbox_default__text">
                            {
                              multiple &&
                              <>
                                <input type="checkbox" checked={optConfig.checked} readOnly={true}/>
                                <i className="checkbox_default__icon"/>
                              </>
                            }
                            {optConfig.displayValue}
                          </label>
                        </div>
                      </div>
                    )
                  }
                ).valueSeq()
              ) : (
                <div key={2} className="no-options">No options</div>
              )}
            </div>
          </div>
        )}

      </>
    )
  };
}
